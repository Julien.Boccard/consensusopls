function str1 = addcode(str,code,deb_end)
%addcode			- adds a string before or after a matrix of characters
%function str1 = addcode(str,code,(deb_end))
%
%Input argument: 
%==============
%str: a matrix of character (n x p)
%code: a string (1 x k)
%deb_end: a number (0= addition before; 1 = addition after; default : 0)
%Output argument:
%===============
%str1 : a matrix of character ((n x (k+p))
%
% This function is mainly used to recode the identifiers of observations or
% variables (".i", or ".v")
% example: 
% data.i
% ans =
% casein 
% albumin
% zein   
% >> data.i=addcode(data.i,'1')
% data = 
%     i: [3x8 char]
% >> data.i
% ans =
% 1casein 
% 1albumin
% 1zein   

[nrow ncol]=size(str);
if(nargin<3)
    deb_end=0;
end

if deb_end==0;%addition before (default)
[nrow ncol]=size(str);
end

if(isempty(code))
    str1=str;
else
    aux=char(ones(nrow,1)*code);
    if(deb_end==0)
        str1=[aux str];
    else
        str1=[str aux];
    end
end      