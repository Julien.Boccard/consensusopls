function t = ssq(a)
%SSQ	SSQ(A) is the sum of squares of the elements of matrix A.
t = sum(sum(a.^2));
