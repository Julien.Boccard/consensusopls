function [saisir] = matrix2saisir(data,coderow,codecol)
%matrix2saisir 	- transforms a Matlab matrix in a saisir structure
%X = matrix2saisir(data,(coderow),(codecol))
%
%Input arguments:
%---------------
%data : Matlab matric
%coderow (optional) : string (code added to the row identifiers) 
%codecol (optional): string (code added to the variables identifiers)
%
%Output argument:
%----------------
%X: SAISIR matrix with fields "d" (copy of "data"), "i" (identifiers of
%rows), "v" (identifiers of variables)
%
% Saisir means "statistique appliqu�e � l'interpretation des spectres infrarouge"
%or "statistics applied to the interpretation of IR spectra' 
%
%See the manual of SAISIR for understanding the rationale of this structure. 
%
%%Typical use:
%%===========
%A=[1 2 3 4; 5 6 7 8];
%B=matrix2saisir(A,'row # ', 'Column # ');
% % >> B.d
% % ans =
% %      1     2     3     4
% %      5     6     7     8
% % >> B.i
% % ans =
% % row # 1
% % row # 2
% % >> B.v
% % ans =
% % Column # 1
% % Column # 2
% % Column # 3
% % Column # 4



if(~isnumeric(data));
    error('data is not numeric');
end 

if(nargin<2);coderow='';end;
if(nargin<3);codecol='';end;
[n,p]=size(data);
saisir.d=data;
saisir.i=addcode(num2str((1:n)'),coderow);
saisir.v=addcode(num2str((1:p)'),codecol);

