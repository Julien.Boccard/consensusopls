# consensusOPLS

Consensus Orthogonal Partial Least Squares

Please cite:

A consensus OPLS-DA strategy for multiblock Omics data fusion. 
J. Boccard, D.N. Rutledge. Analytica Chimica Acta (2013), 769, 30-39.

This package is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY.

Some functions are from the KOPLS package 1.1.0 for MATLAB.
K-OPLS package: Kernel-based orthogonal projections to latent structures for prediction and interpretation in feature space.
Bylesjo M, Rantalainen M, Nicholson JK, Holmes E and Trygg J. BMC Bioinformatics (2008), 9(106).